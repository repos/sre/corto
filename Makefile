# SPDX-License-Identifier: Apache-2.0
# Copyright 2024 Wikimedia Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CORTO_CONFIG_PATH ?= "$(PWD)/config.yaml"

VERSION     = $(shell /usr/bin/git describe --always)
BUILD_DATE  = $(shell date -Iseconds)
HOSTNAME    = $(shell hostname)

GO_PACKAGES := ./...

GO_LDFLAGS  = -X main.version=$(if $(VERSION),$(VERSION),unknown)
GO_LDFLAGS += -X main.buildDate=$(if $(BUILD_DATE),$(BUILD_DATE),unknown)
GO_LDFLAGS += -X main.buildHost=$(if $(HOSTNAME),$(HOSTNAME),unknown)


build:
	go build -ldflags "$(GO_LDFLAGS)" -o . $(GO_PACKAGES)

	@echo
	@echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	@echo "VERSION ......: $(VERSION)"
	@echo "BUILD HOST ...: $(HOSTNAME)"
	@echo "BUILD DATE ...: $(BUILD_DATE)"
	@echo "GO VERSION ...: $(word 3, $(shell go version))"
	@echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

clean:
	@rm -f cortobot

install: build
	install -D -m 755 cortobot $$DESTDIR/usr/bin/cortobot

check:
	go vet $(GO_PACKAGES)

test: check unit-test

unit-test:
	go test --tags=unit -v $(GO_PACKAGES)

functional-test:
	CORTO_CONFIG_PATH=$(CORTO_CONFIG_PATH) go test --tags=functional -v $(GO_PACKAGES)


.PHONY: build check clean functional-test install test unit-test
