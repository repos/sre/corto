// SPDX-License-Identifier: Apache-2.0
// Copyright 2024 Wikimedia Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"regexp"

	corto "gitlab.wikimedia.org/repos/sre/corto/v1"
)

var (
	confFlag = flag.String("config", "config.yaml", "Path to configuration file")
	versFlag = flag.Bool("version", false, "Print version information")

	// These values are passed in at build time using -ldflags
	version   = "unknown"
	buildHost = "unknown"
	buildDate = "unknown"
)

func useDebug() bool {
	s := os.Getenv("CORTO_DEBUG")
	match, _ := regexp.MatchString(`[Yy][Ee][Ss]|1|[Tt][Rr][Uu][Ee]`, s)

	return match
}

func init() {
	flag.Parse()
}

func main() {
	var bot *ircbot
	var cfg *corto.Config
	var cortoObj *corto.Corto
	var ctx context.Context = context.Background()
	var err error
	var googleDoc *corto.GoogleDocument
	var tasks corto.TaskManager

	// If we were invoked w/ -version
	if *versFlag {
		fmt.Fprintf(os.Stderr, "%s (build host: %s, date: %s)\n", version, buildHost, buildDate)
		os.Exit(0)
	}

	logError := func(msg string, args ...any) {
		fmt.Fprintf(os.Stderr, msg+"\n", args...)
	}

	if cfg, err = corto.NewConfig(*confFlag); err != nil {
		logError("Unable to load configuration from file: %v", err)
		os.Exit(1)
	}

	if tasks, err = corto.NewManiphestTaskManager(
		cfg.Phabricator.Token,
		cfg.Phabricator.URL,
		cfg.Phabricator.Project,
		cfg.Phabricator.Policy.View,
		cfg.Phabricator.Policy.Edit,
	); err != nil {
		logError("Unable to create Phabricator client: %s", err)
		os.Exit(1)
	}

	if googleDoc, err = corto.NewGoogleDocument(ctx, cfg.GoogleDriveId, cfg.GoogleDriveCredsPath); err != nil {
		logError("Unable to create GoogleDocument instance: %s", err)
		os.Exit(1)
	}

	cortoObj, err = corto.NewCorto(cfg, tasks, googleDoc)
	if err != nil {
		logError("Unable to create Corto instance: %v", err)
		os.Exit(1)
	}

	ircconf := cfg.IrcConfig
	bot = newBot(ircconf.NickName, ircconf.Password, ircconf.Channels, true, useDebug(), cortoObj)

	bot.Log().Printf("cortobot version %s, (build host: %s, date: %s)", version, buildHost, buildDate)
	bot.Connect(ircconf.ServerName, ircconf.ServerPort)
	bot.Loop()
}
