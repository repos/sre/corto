// SPDX-License-Identifier: Apache-2.0
// Copyright 2024 Wikimedia Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"crypto/tls"
	"errors"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	irc "github.com/thoj/go-ircevent"
	corto "gitlab.wikimedia.org/repos/sre/corto/v1"
)

type ircbot struct {
	conn     *irc.Connection
	cortoObj *corto.Corto
	channels []string
}

func (b *ircbot) Connect(server string, port uint) error {
	return b.conn.Connect(fmt.Sprintf("%s:%d", server, port))
}

func (b *ircbot) Log() *log.Logger {
	return b.conn.Log
}

func (b *ircbot) Loop() {
	// Start the connection loop...
	go func() { b.conn.Loop() }()

	// Block until receiving a shutdown signal
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	s := <-done

	b.conn.Log.Printf("Shutting down (signal=%s)...", s.String())
	b.conn.Quit()
}

func (b *ircbot) dispatch(e *irc.Event) {
	var dest = e.Arguments[0] // Should be either us (irccon.GetNick(), or one of the channels we joined.
	var fields = strings.Fields(e.MessageWithoutFormat())
	var arguments []string
	var cmd string
	var replyTo string

	// Suss out whether someone is talking to us —and if so, from where (i.e. where does the reply go)— and
	// what the command and arguments are.

	var normalizeCommand = func(s string) string { return strings.TrimPrefix(s, "!") }

	// Did the OP message us directly...?
	if dest == b.conn.GetNick() {
		replyTo = e.Nick

		// Someone messaged us whitespace
		if len(fields) < 1 {
			b.conn.Privmsgf(replyTo, "eh!?")
			return
		}

		cmd = normalizeCommand(fields[0])
		arguments = fields[1:]

	} else {
		// ...or did the OP send it to one of our channels, but tag us?
		for _, channel := range b.channels {
			if dest == channel && fields[0] == fmt.Sprintf("%s:", b.conn.GetNick()) {
				replyTo = channel

				// We were poked, but no command was supplied.
				if len(fields) < 2 {
					b.conn.Privmsgf(replyTo, "%s: eh!?", e.Nick)
					return
				}

				cmd = normalizeCommand(fields[1])
				arguments = fields[2:]
				break
			}
		}

		// This message wasn't meant for us, ignore it.
		if cmd == "" {
			return
		}
	}

	switch strings.ToUpper(cmd) {
	case "CREATE":
		b.create(replyTo, arguments)
		return

	case "LIST":
		b.list(replyTo)
		return

	case "RESOLVE", "CLOSE":
		b.resolve(replyTo, arguments)
		return

	case "SET-COORDINATOR", "SET-IC":
		b.set_coordinator(replyTo, cmd, arguments)
		return

	case "HELP":
		b.conn.Privmsgf(replyTo, "You can address me in-channel (for example: %s: help), or message me directly (for example: /msg %s help)", b.conn.GetNick(), b.conn.GetNick())
		b.conn.Privmsgf(replyTo, "Commands that I support:")
		b.conn.Privmsgf(replyTo, "create <title> — Open a new incident.")
		b.conn.Privmsgf(replyTo, "list — List open incidents.")
		b.conn.Privmsgf(replyTo, "resolve <phab ID> — Mark an open incident resolved.")
		b.conn.Privmsgf(replyTo, "set-coordinator <phab ID> <phab user> — (Re)assign the incident coordinator.")
		b.conn.Privmsgf(replyTo, "See also: https://wikitech.wikimedia.org/wiki/Corto")
		return

	case "VERSION":
		b.conn.Privmsgf(replyTo, "cortobot %s (%s)", version, buildDate)
		return

	default:
		b.conn.Privmsgf(replyTo, "Unknown command: %s %v", cmd, arguments)
	}
}

// Create incident
func (b *ircbot) create(r string, args []string) {
	var err error
	var task *corto.Task

	if task, err = b.cortoObj.CreateIncident(strings.Join(args, " ")); err != nil {
		b.Log().Printf("Creating new incident failed: %s", err)
		b.conn.Privmsgf(r, "Creating new incident failed (hint: check the logs)")
		return
	}

	b.conn.Privmsgf(r, "Created new incident: %s", task.URL)
}

// List open incidents
func (b *ircbot) list(r string) {
	var err error
	var task *corto.Task
	var tasks []*corto.Task

	if tasks, err = b.cortoObj.ListOpen(); err != nil {
		b.Log().Printf("Listing open incidents failed: %s", err)
		b.conn.Privmsgf(r, "Listing open incidents failed (hint: check the logs)")
		return
	}

	if len(tasks) > 0 {
		for _, task = range tasks {
			b.conn.Privmsgf(r, "%s: %s", task.URL, task.Title)
		}
	} else {
		b.conn.Privmsg(r, "No 'In Progress' incidents found")
	}
}

// Resolve an incident
func (b *ircbot) resolve(r string, args []string) {
	var err error
	var id int
	var task *corto.Task

	if len(args) != 1 {
		b.conn.Privmsgf(r, "Wrong number of arguments to `resolve'")
		return
	}

	idArg := strings.TrimPrefix(args[0], "T")

	if id, err = strconv.Atoi(idArg); err != nil {
		b.conn.Privmsgf(r, "%s is not a valid argument to `resolve'", args[0])
		return
	}

	if task, err = b.cortoObj.ResolveIncident(id); err != nil {
		var userErr *corto.UserError
		switch {
		case errors.As(err, &userErr):
			b.conn.Privmsgf(r, "Cannot comply: %s", userErr.Error())
		default:
			b.Log().Printf("Resolving of T%d failed: %s", id, err)
			b.conn.Privmsgf(r, "Resolving incident %s failed (hint: check the logs)", args[0])
		}
		return
	}

	b.conn.Privmsgf(r, "Resolved %s", task.URL)

}

// (Re)assign incident
func (b *ircbot) set_coordinator(r, cmd string, args []string) {
	var err error
	var id int
	var task *corto.Task

	if len(args) != 2 {
		b.conn.Privmsgf(r, "Wrong number of arguments to `%s'", cmd)
		return
	}

	idArg := strings.TrimPrefix(args[0], "T")
	nameArg := args[1]

	if id, err = strconv.Atoi(idArg); err != nil {
		b.conn.Privmsgf(r, "%s is not a valid argument to `resolve'", args[0])
		return
	}

	if task, err = b.cortoObj.UpdateIC(id, nameArg); err != nil {
		var userErr *corto.UserError
		switch {
		case errors.As(err, &userErr):
			b.conn.Privmsgf(r, "Cannot comply: %s", userErr.Error())
		default:
			b.Log().Printf("Setting coordinator of T%d to %s failed: %s", id, nameArg, err)
			b.conn.Privmsgf(r, "Setting the coordinator of T%d failed (hint: check the logs)", id)
		}
		return
	}

	b.conn.Privmsgf(r, "%s is now the incident coordinator of %s", nameArg, task.URL)
}

func newBot(nick, password string, channels []string, useTLS bool, debug bool, cortoObj *corto.Corto) *ircbot {
	var bot *ircbot
	var conn = irc.IRC(nick, nick)
	conn.Password = password
	conn.UseTLS = useTLS
	conn.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	conn.VerboseCallbackHandler = debug
	conn.Debug = debug

	// 001: RPL_WELCOME
	conn.AddCallback("001", func(e *irc.Event) {
		for _, channel := range channels {
			conn.Join(channel)
		}
	})

	bot = &ircbot{conn: conn, cortoObj: cortoObj, channels: channels}

	conn.AddCallback("PRIVMSG", bot.dispatch)

	return bot
}
