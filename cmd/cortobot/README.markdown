cortobot
========

An IRC bot to assist with incident coordination at the WMF.

## Running

1. Create a configuration file as appropriate (see: `config.sample.yaml`)
1. Start the bot: `./cortobot -config config.yaml`

The bot will log to stdout.  To increase log verbosity, pass the `CORTO_DEBUG` environment
variable.  For example:

    $ CORTO_DEBUG=1 ./cortobot -config config.yaml


## Gotchas

### Unable to join a channel with +i

Make sure you have operator status on the channel in question, that the bot is configured for
a nick that has been registered with services, and that it is authenticated/identified (see:
`irc_config.nick` & `irc_config.password` in `config.sample.yaml`), and issue:

    /mode #channel +I $a:nickname

