// SPDX-License-Identifier: Apache-2.0
// Copyright 2024 Wikimedia Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package v1

import (
	"fmt"
	"net/url"
	"os"

	yaml "github.com/goccy/go-yaml"
)

// Compatible w/ github.com/lavagetto/ircbot/bot/Configuration
type ircConfig struct {
	ServerName string   `yaml:"server"`
	ServerPort uint     `yaml:"port"`
	UseTLS     bool     `yaml:"use_tls"`
	UseSASL    bool     `yaml:"use_sasl"`
	NickName   string   `yaml:"nick"`
	Password   string   `yaml:"password"`
	Channels   []string `yaml:"channels"`
}

// Config represents a Corto configuration.
type Config struct {
	GoogleDriveId        string    `yaml:"google_drive_id"`
	GoogleDriveCredsPath string    `yaml:"google_drive_creds_path"`
	IrcConfig            ircConfig `yaml:"irc_config"`
	Phabricator          struct {
		Project string `yaml:"project"`
		URL     string `yaml:"url"`
		Token   string `yaml:"token"`
		User    string `yaml:"user"`
		Policy  struct {
			View string `yaml:"view"`
			Edit string `yaml:"edit"`
		}
	} `yaml:"phabricator"`
}

// NewConfig returns a Config struct from the contents of a YAML-formated file.
func NewConfig(filename string) (*Config, error) {
	var data []byte
	var err error

	if data, err = os.ReadFile(filename); err != nil {
		return nil, fmt.Errorf("reading configuration from file: %w", err)
	}

	return ReadConfig(data)
}

// ReadConfig returns a Config struct from the contents of a byte array.
func ReadConfig(data []byte) (*Config, error) {
	var config Config

	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, fmt.Errorf("unmarshalling yaml: %w", err)
	}

	return validateConfig(&config)
}

func validateConfig(config *Config) (*Config, error) {
	if config.GoogleDriveId == "" {
		return nil, fmt.Errorf("missing configuration parameter: google_drive_id")
	}
	if config.GoogleDriveCredsPath == "" {
		return nil, fmt.Errorf("missing configuration parameter: google_drive_creds_path")
	}
	if config.Phabricator.Project == "" {
		return nil, fmt.Errorf("missing configuration parameter: phabricator.project")
	}
	if config.Phabricator.Token == "" {
		return nil, fmt.Errorf("missing configuration parameter: phabricator.token")
	}
	if _, err := url.ParseRequestURI(config.Phabricator.URL); err != nil {
		return nil, fmt.Errorf("unable to parse phabricator.url: %w", err)
	}
	if config.Phabricator.User == "" {
		return nil, fmt.Errorf("missing configuration parameter: phabricator.user")
	}
	if config.Phabricator.Policy.View == "" {
		return nil, fmt.Errorf("missing configuration parameter: phabricator.policy.view")
	}
	if config.Phabricator.Policy.Edit == "" {
		return nil, fmt.Errorf("missing configuration parameter: phabricator.policy.edit")
	}
	if config.IrcConfig.NickName == "" {
		return nil, fmt.Errorf("missing configuration parameter: irc_config.nick")
	}
	if len(config.IrcConfig.Channels) == 0 {
		return nil, fmt.Errorf("missing configuration parameter: irc_config.channels")
	}

	return config, nil
}
