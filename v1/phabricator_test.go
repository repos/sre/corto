// SPDX-License-Identifier: Apache-2.0
// Copyright 2024 Wikimedia Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//go:build functional || !unit
// +build functional !unit

package v1

import (
	"fmt"
	"math/rand"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func randString(size int) string {
	var alnums = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
	var runes = make([]rune, size)

	for i := range runes {
		runes[i] = alnums[rand.Intn(len(alnums))]
	}

	return string(runes)
}

func getTestTaskManager() (*ManiphestTaskManager, error) {
	var conf *Config
	var err error
	var path = os.Getenv("CORTO_CONFIG_PATH")

	if conf, err = NewConfig(path); err != nil {
		return nil, fmt.Errorf("unable to create config object: %w", err)
	}

	return NewManiphestTaskManager(
		conf.Phabricator.Token,
		conf.Phabricator.URL,
		conf.Phabricator.Project,
		conf.Phabricator.Policy.View,
		conf.Phabricator.Policy.Edit,
	)
}

func TestTaskManagement(t *testing.T) {
	var err error
	var task *Task
	var tasks []*Task
	var manager TaskManager

	var title = fmt.Sprintf("Corto automated test — %s", randString(12))
	var descr = "While performing routine maintenance, we accidentally the whole thing!"

	manager, err = getTestTaskManager()
	require.NoError(t, err)

	// Create
	task, err = manager.Create(title, descr)
	require.NoError(t, err)
	assert.NotEmpty(t, task.ID)
	t.Logf("Created task %s", task.URL)
	assert.NotEmpty(t, task.Started)
	assert.Equal(t, title, task.Title)
	assert.Equal(t, StatusInProgress, task.Status)
	assert.Equal(t, descr, task.Description)
	assert.Equal(t, "", task.Coordinator) // IC is unassigned for new Tasks
	// FIXME: validate tags?
	// FIXME: validate URL?

	// List
	tasks, err = manager.ListOpen()
	require.NoError(t, err)
	assert.GreaterOrEqual(t, len(tasks), 1)

	var id int = task.ID

	// Get
	task, err = manager.Get(id)
	require.NoError(t, err)
	assert.Equal(t, id, task.ID)
	// FIXME: more validation is needed

	// AddComment
	err = manager.AddComment(task.ID, "The rain in Spain is falling on the plains.")
	require.NoError(t, err)

	// ListComments
	var comments []TaskComment
	comments, err = manager.ListComments(task.ID, "jhathaway")
	require.NoError(t, err)
	require.Len(t, comments, 1)
	assert.Equal(t, "The rain in Spain is falling on the plains.", comments[0].Content)

	// Assign
	task, err = manager.Assign(task.ID, "jhathaway")
	require.NoError(t, err)
	assert.Equal(t, "jhathaway", task.Coordinator)

	// Resolve
	task, err = manager.Resolve(task.ID)
	require.NoError(t, err)
	assert.Equal(t, statusFor("open"), task.Status)
}

func TestLookupPHID(t *testing.T) {
	var err error
	var manager *ManiphestTaskManager
	var phid string

	manager, err = getTestTaskManager()
	require.NoError(t, err)

	phid, err = manager.LookupPHID(manager.projectName)
	require.NoError(t, err)

	assert.Contains(t, phid, manager.projectPHID)
}
