// SPDX-License-Identifier: Apache-2.0
// Copyright 2024 Wikimedia Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//go:build functional || !unit
// +build functional !unit

package v1

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDocumentCreation(t *testing.T) {
	var err error
	var lnk string

	path := os.Getenv("CORTO_CONFIG_PATH")
	conf, err := NewConfig(path)
	require.NoError(t, err)

	ctx := context.Background()
	doc, err := NewGoogleDocument(ctx, conf.GoogleDriveId, conf.GoogleDriveCredsPath)
	require.NoError(t, err)

	lnk, err = doc.Create(Task{
		ID:          1,
		URL:         "https://phabricator.wikimedia.org/T1",
		Started:     time.Now(),
		Title:       "Site down, problem unknown!",
		Status:      StatusOpen,
		Description: "Fire, fire, everywhere!",
		Coordinator: "testcoord",
	})
	require.NoError(t, err)
	t.Logf("Google doc: %s", lnk)
	assert.Contains(t, lnk, "https://")
}
