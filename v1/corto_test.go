// SPDX-License-Identifier: Apache-2.0
// Copyright 2024 Wikimedia Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//go:build unit || !functional
// +build unit !functional

package v1

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type metadataParseTest struct {
	markdown string
	expected metadata
}

var metadataParseTests = []metadataParseTest{
	{
		`##### Incident metadata

- [Google Doc](http://gdoc.com)
`,
		metadata{
			GdocURI: "http://gdoc.com",
		},
	},
}

func TestMetadataParse(t *testing.T) {
	for _, test := range metadataParseTests {
		output, err := parseMetadata([]byte(test.markdown))
		assert.NoError(t, err)
		assert.Equal(t, test.expected, output)
	}
}
