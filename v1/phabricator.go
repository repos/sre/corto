// SPDX-License-Identifier: Apache-2.0
// Copyright 2024 Wikimedia Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package v1

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/thought-machine/gonduit"
	"github.com/thought-machine/gonduit/core"
	"github.com/thought-machine/gonduit/entities"
	"github.com/thought-machine/gonduit/requests"
	"github.com/thought-machine/gonduit/responses"
)

type status int
type priority int

const (
	StatusOpen status = iota
	StatusResolved
	StatusStalled
	StatusInvalid
	StatusDeclined
	StatusInProgress

	PriorityHigh priority = iota
	PriorityUnbreakNow
	PriorityNeedsTriage
	PriorityMedium
	PriorityLow
)

// Task represents a task ("ticket", etc) in a task tracking system.
type Task struct {
	ID          int       // incident ID (i.e. the task ID)
	Started     time.Time // incident start time (i.e. task open)
	Title       string    // self explanatory
	Status      status    // the incident status (open, closed, etc)
	Description string    // corresponds with ticket description
	Coordinator string    // IC; coordinates w/ "assigned"
	Tags        []string  // ???
	URL         string    // Task URL
}

type ByID []*Task

func (b ByID) Len() int           { return len(b) }
func (b ByID) Swap(i, j int)      { b[i], b[j] = b[j], b[i] }
func (b ByID) Less(i, j int) bool { return b[i].ID < b[j].ID }

// TaskComment represents a comment associated with a Task.
type TaskComment struct {
	Author  string
	Created time.Time
	Updated time.Time
	Content string
}

// TaskManager defines an interface for task managment implementations (ticketing systems).
type TaskManager interface {
	Create(title, description string) (*Task, error)
	Resolve(id int) (*Task, error)
	ListOpen() ([]*Task, error)
	Get(id int) (*Task, error)
	Assign(id int, user string) (*Task, error)
	AddComment(id int, comment string) error
	ListComments(id int, author string) ([]TaskComment, error)
}

// UserError is an error type that represents an invalid request, rather than a general system
// error.  For example, attempting an edit to an issue not under the scope of incident management.
type UserError struct {
	Msg string
}

func (u *UserError) Error() string {
	return u.Msg
}

// ManiphestTaskManager is a Phabricator implementation of the TaskManager interface.
type ManiphestTaskManager struct {
	URL         string
	projectName string
	projectPHID string
	policyPHIDs struct {
		edit string
		view string
	}
	conn *gonduit.Conn
}

// Create creates a new (open) task given a title and description.  A corresponding Task struct is
// returned upon success, or an error otherwise.
func (p *ManiphestTaskManager) Create(title, description string) (*Task, error) {
	var err error
	var response *responses.EditResponse

	var request = requests.EditRequest{Transactions: []entities.Transaction{
		{Type: "title", Value: title},
		{Type: "description", Value: description},
		{Type: "status", Value: statusString(StatusInProgress)},
		{Type: "projects.add", Value: []string{p.projectPHID}},
		{Type: "priority", Value: priorityString(PriorityHigh)},
		{Type: "view", Value: p.policyPHIDs.view},
		{Type: "edit", Value: p.policyPHIDs.edit},
	}}

	if response, err = p.conn.ManiphestEdit(request); err != nil {
		return nil, fmt.Errorf("failed to create new incident: %w", err)
	}

	// Create a by-PHID search request, and lookup the newly created Incident
	phid := response.Object.PHID
	search := requests.ManiphestQueryRequest{PHIDs: []string{string(phid)}}

	return p.lookup(search)
}

// Resolve moves an existing Task (corresponding to its ID) from
// StatusInProgress to StatusOpen. After changing the status it also adds a
// comment, indicating that the incident is resolved, but the task has been
// moved to StatusOpen in order to track any followup work. Upon success, the
// corresponding Task is returned, otherwise an error.
func (p *ManiphestTaskManager) Resolve(id int) (*Task, error) {
	var err error
	var request = requests.EditRequest{
		ObjectIdentifier: entities.ObjectIdentifier{ID: id},
		Transactions:     []entities.Transaction{{Type: "status", Value: statusString(StatusOpen)}},
	}

	// Make sure this incident is one of ours.
	if err = p.validateOurs(id); err != nil {
		return nil, err
	}

	if _, err = p.conn.ManiphestEdit(request); err != nil {
		return nil, fmt.Errorf("failed to resolve incident: %w", err)
	}

	if err = p.AddComment(
		id,
		"Incident has been resolved, moving status from 'In Progress' to"+
			" 'Open' so that followup work can be tracked via this task.",
	); err != nil {
		return nil, fmt.Errorf("failed to add comment to resolved incident: %w", err)
	}

	return p.Get(id)
}

// ListOpen returns a list of open tasks.
func (p *ManiphestTaskManager) ListOpen() ([]*Task, error) {
	var err error
	var response *responses.ManiphestQueryResponse
	var incidents = make([]*Task, 0)
	// We only want tasks with the status of 'In Progress' however, Maniphest
	// query only allows you to query for a specific set of statuses, which
	// excludes custom status, such as 'In Progress'. We query for `status-open` since
	// a 'In Progress' task is also open:
	// - https://phabricator.wikimedia.org/conduit/method/maniphest.query/
	var request = requests.ManiphestQueryRequest{ProjectPHIDs: []string{p.projectPHID}, Status: "status-open"}

	if response, err = p.conn.ManiphestQuery(request); err != nil {
		return nil, fmt.Errorf("unable to list open incidents (%s): %s", p.URL, err)
	}

	for _, task := range *response {
		var incident *Task
		if incident, err = p.taskFromManiphest(task); err != nil {
			return nil, fmt.Errorf("error processing list of open incidents: %w", err)
		}

		// We only want tasks with the status 'In Progress' as this status
		// indicates an incident that is ongoing.
		if incident.Status == StatusInProgress {
			incidents = append(incidents, incident)
		}
	}

	return incidents, nil
}

// Get returns a Task corresponding to its ID.
func (p *ManiphestTaskManager) Get(id int) (*Task, error) {
	// Lookup the Incident using a by-ID search request
	request := requests.ManiphestQueryRequest{IDs: []string{strconv.Itoa(id)}}
	return p.lookup(request)
}

func (p *ManiphestTaskManager) lookup(req requests.ManiphestQueryRequest) (*Task, error) {
	var err error
	var response *responses.ManiphestQueryResponse

	if response, err = p.conn.ManiphestQuery(req); err != nil {
		return nil, err
	}

	// There should be only one...
	for _, task := range *response {
		return p.taskFromManiphest(task)
	}

	return nil, fmt.Errorf("no task with matching req (%+v)", req)
}

func (p *ManiphestTaskManager) taskFromManiphest(task *entities.ManiphestTask) (*Task, error) {
	var err error
	var id int
	var owner string

	if id, err = strconv.Atoi(task.ID); err != nil {
		return nil, fmt.Errorf("unable to coerce task ID (%s) to int: %w", task.ID, err)
	}

	// Tasks can be unassigned...
	if task.OwnerPHID != "" {
		if owner, err = p.QueryPHID(task.OwnerPHID); err != nil {
			return nil, fmt.Errorf("unable to lookup PHID for assignee: %w", err)
		}
	}

	return &Task{
		ID:          id,
		Started:     time.Time(task.DateCreated),
		Title:       task.Title,
		Status:      statusFor(task.Status),
		Description: task.Description,
		Coordinator: owner,
		Tags:        task.ProjectPHIDs, // XXX: Is this right?
		URL:         fmt.Sprintf("%s/T%d", p.URL, id),
	}, nil
}

// Assign (re)assigns the owner of a Task
func (p *ManiphestTaskManager) Assign(id int, user string) (*Task, error) {
	var err error

	var request = requests.EditRequest{
		ObjectIdentifier: entities.ObjectIdentifier{ID: id},
		Transactions:     []entities.Transaction{{Type: "owner", Value: user}},
	}

	// Make sure this incident is one of ours.
	if err = p.validateOurs(id); err != nil {
		return nil, err
	}

	if _, err = p.conn.ManiphestEdit(request); err != nil {
		return nil, fmt.Errorf("unable to (re)assign task to %s: %w", user, err)
	}

	return p.Get(id)
}

// AddComment creates a new comment on the Task associated with the id argument, using content
// corresponding to the comment arg.
func (p *ManiphestTaskManager) AddComment(id int, comment string) error {
	var err error
	var request = requests.EditRequest{
		ObjectIdentifier: entities.ObjectIdentifier{ID: id},
		Transactions:     []entities.Transaction{{Type: "comment", Value: comment}},
	}

	// Make sure this incident is one of ours.
	if err = p.validateOurs(id); err != nil {
		return err
	}

	if _, err = p.conn.ManiphestEdit(request); err != nil {
		return fmt.Errorf("unable to add comment: %w", err)
	}

	return nil
}

// ListComments returns a list of TaskComments according to a Task id and author.
func (p *ManiphestTaskManager) ListComments(id int, author string) ([]TaskComment, error) {
	var authorID = ternary(strings.HasPrefix(author, "@"), author, fmt.Sprintf("@%s", author))
	var authorPHID string
	var comments []TaskComment
	var err error

	// Map the author to a Phabricator PHID.
	if authorPHID, err = p.LookupPHID(authorID); err != nil {
		return nil, fmt.Errorf("unable to lookup PHID for %s: %w", authorID, err)
	}

	var request = requests.TransactionSearchRequest{
		ObjectID: fmt.Sprintf("T%d", id),
		Limit:    100,
		Constraints: map[string]interface{}{
			"authorPHIDs": [...]string{authorPHID},
		},
	}
	var response *responses.TransactionSearchResponse

Outer:
	for {
		if response, err = p.conn.TransactionSearch(request); err != nil {
			err = fmt.Errorf("query of comments for %d (author: %s) failed: %w", id, authorID, err)
			break Outer
		}

		if len(response.Data) == 0 {
			break Outer
		}

		// Loop through all transactions
		for _, t := range response.Data {
			switch t.Type {
			case "comment":
				if len(t.Comments) == 0 {
					continue
				}

				comments = append(comments, TaskComment{
					Author:  t.AuthorPHID,
					Created: time.Time(t.DateCreated),
					Updated: time.Time(t.DateModified),
					Content: t.Comments[0].Content["raw"].(string),
				})
			}
		}

		if response.Cursor.After == nil {
			break Outer
		}

		// Loop around for another page of results?
		request.After = response.Cursor.After.(string) // XXX: ???
	}

	return comments, err
}

// LookupPHID returns the Phabricator PHID for a name.
func (p *ManiphestTaskManager) LookupPHID(name string) (string, error) {
	var err error
	var req = requests.PHIDLookupRequest{Names: []string{name}}
	var res responses.PHIDLookupResponse

	if res, err = p.conn.PHIDLookup(req); err != nil {
		return "", fmt.Errorf("unable to lookup PHID for %s: %w", name, err)
	}

	// There should only be one...
	for _, v := range res {
		return v.PHID, err
	}

	return "", fmt.Errorf("no PHID found for %s", name)
}

func (p *ManiphestTaskManager) QueryPHID(phid string) (string, error) {
	var err error
	var req = requests.PHIDQueryRequest{PHIDs: []string{phid}}
	var res responses.PHIDQueryResponse

	if res, err = p.conn.PHIDQuery(req); err != nil {
		return "", fmt.Errorf("unable to query name for PHID %s: %w", phid, err)
	}

	// Expecting only one...
	for _, v := range res {
		return v.Name, nil
	}

	return "", fmt.Errorf("no results found for PHID %s", phid)
}

// We don't want Corto making changes to tickets outside of its remit;  Returns a UserError
// if the Maniphest task corresponding to ID isn't tagged with the configured project.
func (p *ManiphestTaskManager) validateOurs(id int) error {
	var err error
	var task *Task

	if task, err = p.Get(id); err != nil {
		return fmt.Errorf("unable to lookup T%d for validation: %w", id, err)
	}

	for _, tag := range task.Tags {
		if tag == p.projectPHID {
			return nil
		}
	}

	return &UserError{fmt.Sprintf("T%d is not tagged %s", id, p.projectName)}
}

// NewManiphestTaskManager creates a PhabricatorIncidents from a token, URL, and project name.
func NewManiphestTaskManager(token, url, project, viewPolicy, editPolicy string) (*ManiphestTaskManager, error) {
	var conn *gonduit.Conn
	var err error
	var projectName = ternary(strings.HasPrefix("#", project), project, fmt.Sprintf("#%s", project))
	var projectPHID string
	var taskManager *ManiphestTaskManager
	var view, edit string

	phabOptions := &core.ClientOptions{APIToken: token}
	if conn, err = gonduit.Dial(url, phabOptions); err != nil {
		return nil, fmt.Errorf("unable to connect to %s: %w", url, err)
	}

	taskManager = &ManiphestTaskManager{URL: url, projectName: projectName, conn: conn}

	// Map human-readable names to Phabricator PHIDs as necessary.
	if projectPHID, err = taskManager.LookupPHID(projectName); err != nil {
		return nil, fmt.Errorf("unable to map project name (%s) to PHID: %w", projectName, err)
	}
	if view, err = taskManager.LookupPHID(viewPolicy); err != nil {
		return nil, fmt.Errorf("unable to map view policy name (%s) to PHID: %w", viewPolicy, err)
	}
	if edit, err = taskManager.LookupPHID(editPolicy); err != nil {
		return nil, fmt.Errorf("unable to map edit policy name (%s) to PHID: %w", editPolicy, err)
	}

	taskManager.projectPHID = projectPHID
	taskManager.policyPHIDs.edit = edit
	taskManager.policyPHIDs.view = view

	return taskManager, nil
}

//lint:ignore U1000 mockTaskManager is not (yet) being used.
type mockTaskManager struct {
	id        int
	incidents map[int]Task
	comments  map[int][]TaskComment
}

func (m *mockTaskManager) Create(title, description string) (*Task, error) {
	m.id += 1
	incident := Task{
		ID:          m.id,
		Started:     time.Now(),
		Title:       title,
		Status:      StatusOpen,
		Description: description,
		Coordinator: "epresley", // FIXME: hardcoded!
	}
	m.incidents[m.id] = incident
	return &incident, nil
}

func (m *mockTaskManager) Resolve(id int) error {
	delete(m.incidents, id)
	delete(m.comments, id)
	return nil
}

func (m *mockTaskManager) ListOpen() ([]*Task, error) {
	var incidents = make([]*Task, 0)
	for _, v := range m.incidents {
		incidents = append(incidents, &v)
	}
	return incidents, nil
}

func (m *mockTaskManager) Get(id int) (*Task, error) {
	val, present := m.incidents[id]
	if present {
		return &val, nil
	}
	return nil, fmt.Errorf("id=%d not found", id)
}

func (m *mockTaskManager) Assign(id int, user string) (*Task, error) {
	val, present := m.incidents[id]
	if present {
		val.Coordinator = user
		return &val, nil
	}
	return nil, fmt.Errorf("unknown task %d", id)
}

func (m *mockTaskManager) AddComment(id int, comment string) error {
	m.comments[id] = append(m.comments[id], TaskComment{Author: "mock", Created: time.Now(), Updated: time.Now(), Content: comment})
	return nil
}

func (m *mockTaskManager) ListComments(id int, author string) ([]TaskComment, error) {
	comments, ok := m.comments[id]
	if !ok {
		return make([]TaskComment, 0), nil
	}
	return comments, nil
}

//lint:ignore U1000 mockTaskManager is not (yet) being used.
func newMockTaskManager() *mockTaskManager {
	return &mockTaskManager{id: 0, incidents: make(map[int]Task), comments: make(map[int][]TaskComment)}
}

// Returns the status int const for a phabricator Conduit API status string
func statusFor(s string) status {
	switch strings.ToUpper(s) {
	case "OPEN":
		return StatusOpen
	case "RESOLVED":
		return StatusResolved
	case "STALLED":
		return StatusStalled
	case "INVALID":
		return StatusInvalid
	case "DECLINED":
		return StatusDeclined
	case "PROGRESS":
		return StatusInProgress
	default:
		return -1
	}
}

// Returns the phabricator Conduit API status string for a given status int const
func statusString(s status) string {
	switch s {
	case StatusOpen:
		return "open"
	case StatusResolved:
		return "resolved"
	case StatusStalled:
		return "stalled"
	case StatusInvalid:
		return "invalid"
	case StatusDeclined:
		return "declined"
	// Custom phabricator status, config:
	//
	// https://<DOMAIN>/config/edit/maniphest.statuses/
	//
	// "progress": {
	//   "claim": false,
	//   "closed": false,
	//   "name": "In Progress",
	//   "name.action": "Started",
	//   "name.full": "Open, In Progress",
	//   "transaction.icon": "fa-step-forward",
	//   "transaction.color": "green"
	// }
	case StatusInProgress:
		return "progress"
	default:
		return "-1"
	}
}

//lint:ignore U1000 priorityFor is not (yet) being used.
func priorityFor(s string) priority {
	switch strings.ToUpper(s) {
	case "HIGH":
		return PriorityHigh
	case "UNBREAK NOW!":
		return PriorityUnbreakNow
	case "NEEDS TRIAGE":
		return PriorityNeedsTriage
	case "MEDIUM":
		return PriorityMedium
	case "LOW":
		return PriorityLow
	default:
		return -1
	}
}

func priorityString(prio priority) string {
	switch prio {
	case PriorityHigh:
		return "high"
	case PriorityUnbreakNow:
		return "unbreak now!"
	case PriorityNeedsTriage:
		return "needs triage"
	case PriorityMedium:
		return "medium"
	case PriorityLow:
		return "low"
	default:
		return "-1"
	}
}
