// SPDX-License-Identifier: Apache-2.0
// Copyright 2024 Wikimedia Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//go:build unit || !functional
// +build unit !functional

package v1

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestConfig(t *testing.T) {
	// Parsing from a file
	c, e := NewConfig("../config.sample.yaml")

	require.NoError(t, e)

	assert.NotEmpty(t, c.GoogleDriveId)
	assert.NotEmpty(t, c.Phabricator.Project)
	assert.NotEmpty(t, c.Phabricator.URL)
	assert.NotEmpty(t, c.Phabricator.Token)

	// Validation testing
	tests := map[string]string{
		"bad phabricator url": `
google_drive_id:        1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
google_drive_creds_path google-creds.json
phabricator:
  project:    Wikimedia-Incident
  url:        https//phabricator.wmcloud.org
  token:      api-1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
  user:       jhathaway
  policy:
    edit: users
    view: public
irc_config:
  nick: cortobot-test
  channels:
    - '#corto-test'
  admins:
    - butter`,

		"missing google drive id": `
google_drive_creds_path google-creds.json
project:    Wikimedia-Incident

phabricator:
  project:    Wikimedia-Incident
  url:        https://phabricator.wmcloud.org
  token:      api-1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
  user:       jhathaway
  policy:
    edit: users
    view: public
irc_config:
  nick: cortobot-test
  channels:
    - '#corto-test'
  admins:
    - butter`,

		"missing phabricator project phid": `
google_drive_id:        1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
google_drive_creds_path google-creds.json
phabricator:
  url:        https://phabricator.wmcloud.org
  token:      api-1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
  user:       jhathaway
  policy:
    edit: users
    view: public
irc_config:
  nick: cortobot-test
  channels:
    - '#corto-test'
  admins:
    - butter`,

		"missing phabricator token": `
google_drive_id:        1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
google_drive_creds_path google-creds.json
phabriactor:
  project:    Wikimedia-Incident
  url:        https://phabricator.wmcloud.org
  user:       jhathaway
  policy:
    edit: users
    view: public

irc_config:
  nick: cortobot-test
  channels:
    - '#corto-test'
  admins:
    - butter`,

		"missing phabricator user": `
google_drive_id:        t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
google_drive_creds_path google-creds.json
project:    Wikimedia-Incident
url:        https://phabricator.wmcloud.org
token:      api-1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
phabriactor:
  project:    Wikimedia-Incident
  url:        https://phabricator.wmcloud.org
  token:      api-1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
  policy:
    edit: users
    view: public
irc_config:
  nick: cortobot-test
  channels:
    - '#corto-test'
  admins:
    - butter`,

		"missing irc nick": `
google_drive_id:        1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
google_drive_creds_path google-creds.json
phabrictor:
  project:    Wikimedia-Incident
  url:        https://phabricator.wmcloud.org
  token:      api-1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
  user:       jhathaway
  policy:
    edit: users
    view: public
irc_config:
  channels:
    - '#corto-test'
  admins:
    - butter`,

		"missing irc channel list": `
google_drive_id:        1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
google_drive_creds_path google-creds.json
phabricator:
  project:    Wikimedia-Incident
  url:        https://phabricator.wmcloud.org
  token:      api-1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
  user:       jhathaway
  policy:
    edit: users
    view: public
irc_config:
  nick: cortobot-test
  admins:
    - butter`,

		"missing irc admin list": `
google_drive_id:        1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
google_drive_creds_path google-creds.json
phabrictor:
  project:    Wikimedia-Incident
  url:        https://phabricator.wmcloud.org
  token:      api-1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
  user:       jhathaway
  policy:
    edit: users
    view: public
irc_config:
  nick: cortobot-test
  channels:
    - '#corto-test'`,
		"missing phabricator view policy": `
google_drive_id:        1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
google_drive_creds_path google-creds.json
phabrictor:
  project:    Wikimedia-Incident
  url:        https://phabricator.wmcloud.org
  token:      api-1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
  user:       jhathaway
  policy:
    edit: users
irc_config:
  nick: cortobot-test
  channels:
    - '#corto-test'`,
		"missing phabricator edit policy": `
google_drive_id:        1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
google_drive_creds_path google-creds.json
phabrictor:
  project:    Wikimedia-Incident
  url:        https://phabricator.wmcloud.org
  token:      api-1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi
  user:       jhathaway
  policy:
    view: public
irc_config:
  nick: cortobot-test
  channels:
    - '#corto-test'`,
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			_, e := ReadConfig([]byte(test))
			assert.Error(t, e)
		})
	}
}
