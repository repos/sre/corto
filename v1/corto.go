// SPDX-License-Identifier: Apache-2.0
// Copyright 2024 Wikimedia Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package v1

import (
	"bytes"
	"fmt"
	"html/template"
	"regexp"
	"sort"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/text"
	"gitlab.wikimedia.org/repos/sre/corto/templates"
)

type Corto struct {
	config         *Config
	googleDocument Document
	templates      *template.Template
	tasks          TaskManager
}

type metadata struct {
	GdocURI string
}

func NewCorto(config *Config, tasks TaskManager, gDoc *GoogleDocument) (*Corto, error) {
	var err error
	var tmpl *template.Template

	if tmpl, err = template.ParseFS(templates.FS, "task.*.tmpl"); err != nil {
		errorStr := "unable to parse templates: %w"
		return nil, fmt.Errorf(errorStr, err)
	}

	return &Corto{config: config, tasks: tasks, googleDocument: gDoc, templates: tmpl}, nil
}

// CreateIncident creates a new incident (Google Doc & Phabricator task), given an incident title.
func (c *Corto) CreateIncident(title string) (*Task, error) {
	var commentBuf bytes.Buffer
	var descrBuf bytes.Buffer
	var err error
	var gdocURL string
	var incident *Task

	if err = c.templates.ExecuteTemplate(&descrBuf, "task.description.tmpl", title); err != nil {
		errorStr := "unable to create task description: %w"
		return nil, fmt.Errorf(errorStr, err)
	}

	// Create a new incident, and retrieve it.
	if incident, err = c.tasks.Create(title, descrBuf.String()); err != nil {
		return nil, err
	}

	// That non-optional part of this transaction is complete; Everything that follows is best-effort.

	go func() {
		// Create a new Google document
		if gdocURL, err = c.googleDocument.Create(*incident); err == nil {
			// Format comment text with a link to the Google document
			if err = c.templates.ExecuteTemplate(&commentBuf, "task.metadata.tmpl", metadata{GdocURI: gdocURL}); err == nil {
				// Add metadata as a task comment, with a link to the Google Doc
				c.tasks.AddComment(incident.ID, commentBuf.String())
			}
		}
	}()

	return incident, nil
}

// ListOpen returns all open Tasks in Phabricator
func (c *Corto) ListOpen() ([]*Task, error) {
	var results []*Task
	var err error

	// Get a list of IDs matching open Phabricator tasks
	if results, err = c.tasks.ListOpen(); err != nil {
		return nil, err
	}

	sort.Sort(ByID(results))

	return results, nil
}

// GetIncident loads an existing Incident from its task ID
func (c *Corto) GetIncident(id int) (*Task, error) {
	return c.tasks.Get(id)
}

func (c *Corto) ResolveIncident(id int) (*Task, error) {
	return c.tasks.Resolve(id)
}

func (c *Corto) TitleIncident(PHID string, new_title string) (bool, error) {
	// task, err := c.findTaskByPHID(PHID)
	// if err != nil {
	// 	return false, fmt.Errorf("Unable to find task: %w", err)
	// }
	// TODO change title
	return true, nil
}

func (c *Corto) UpdateIC(id int, coordinator string) (*Task, error) {
	return c.tasks.Assign(id, coordinator)
}

//lint:ignore U1000 fetchMetadataComment is not (yet) being used.
func (c *Corto) fetchMetadataComment(id int, authorPHID string) (string, error) {
	var comments []TaskComment
	var err error
	var metadata string
	var metadataRegex = regexp.MustCompile("^##### Incident metadata")

	if comments, err = c.tasks.ListComments(id, authorPHID); err != nil {
		for _, comment := range comments {
			if metadataRegex.MatchString(comment.Content) {
				metadata = comment.Content
			}
		}
	}

	return metadata, err
}

//lint:ignore U1000 parseMetadata is not (yet) being used.
func parseMetadata(source []byte) (metadata, error) {
	var meta metadata
	var err error

	parser := goldmark.New().Parser()
	doc := parser.Parse(text.NewReader(source))
	err = ast.Walk(doc, func(node ast.Node, enter bool) (ast.WalkStatus, error) {
		if !enter {
			return ast.WalkContinue, nil
		}
		switch node.(type) {
		case *ast.Text:
			parent := node.Parent()
			text := node.(*ast.Text)
			content := string(text.Segment.Value(source))
			if content == "Google Doc" && parent.Kind() == ast.KindLink {
				parentLink := parent.(*ast.Link)
				meta.GdocURI = string(parentLink.Destination)
			}
		}

		return ast.WalkContinue, nil
	})
	if err != nil {
		return metadata{}, fmt.Errorf("unable to parse metadata: %w", err)
	}
	return meta, nil
}
