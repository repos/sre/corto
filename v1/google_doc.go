// SPDX-License-Identifier: Apache-2.0
// Copyright 2024 Wikimedia Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package v1

import (
	"bytes"
	"context"
	"fmt"
	"html/template"
	"time"

	"gitlab.wikimedia.org/repos/sre/corto/templates"
	"google.golang.org/api/drive/v3"
	"google.golang.org/api/googleapi"
	"google.golang.org/api/option"
)

type Document interface {
	// Create creates a new templated collab document from an Incident
	Create(incident Task) (string, error)
}

type mockDocument struct{}

func (m *mockDocument) Create(incident Task) (string, error) {
	return "https://www2.example.biz/mockDocument#edit", nil
}

type GoogleDocument struct {
	googleDriveID  string
	googleDriveSrv *drive.Service
	templates      *template.Template
}

func (g *GoogleDocument) Create(incident Task) (string, error) {
	var gdoc *drive.File
	var err error

	dateStamp := time.Now().Format("2006-01-02")
	gdocTitle := fmt.Sprintf("%s: %s (T%d)", dateStamp, incident.Title, incident.ID)

	var incidentBuf bytes.Buffer
	err = g.templates.ExecuteTemplate(
		&incidentBuf,
		"task.incident.tmpl",
		struct {
			PhabID  int
			PhabURI string
			Title   string
		}{
			incident.ID,
			incident.URL,
			fmt.Sprintf("%s: %s", dateStamp, incident.Title),
		},
	)
	if err != nil {
		return "", fmt.Errorf("unable to render the template: %w", err)
	}

	gdoc = &drive.File{
		Name:     gdocTitle,
		Parents:  []string{g.googleDriveID},
		MimeType: "application/vnd.google-apps.document",
	}

	files := g.googleDriveSrv.Files

	gdoc, err = files.Create(gdoc).Media(&incidentBuf, googleapi.ContentType("text/markdown")).Do()
	if err != nil {
		return "", fmt.Errorf("unable to create the google doc: %w", err)
	}

	// Re-get our file, so we can grab the webViewLink field, alternatively, we could
	// just construct the url ourselves and hope the format never changes.
	f := []googleapi.Field{"webViewLink"}
	if gdoc, err = files.Get(gdoc.Id).Fields(f...).Do(); err != nil {
		return "", fmt.Errorf("failed to retrieve webview link: %w (id=%s)", err, gdoc.Id)
	}

	return gdoc.WebViewLink, nil
}

func NewGoogleDocument(ctx context.Context, driveID, credsFile string) (*GoogleDocument, error) {
	var err error
	var googleDriveSrv *drive.Service
	var tmpl *template.Template

	if googleDriveSrv, err = drive.NewService(ctx, option.WithCredentialsFile(credsFile)); err != nil {
		return nil, fmt.Errorf("unable to create Google Drive client: %w", err)
	}

	// XXX: Is it a problem for us to create our own Template instance here?
	if tmpl, err = template.ParseFS(templates.FS, "task.*.tmpl"); err != nil {
		errorStr := "unable to parse templates: %w"
		return nil, fmt.Errorf(errorStr, err)
	}

	return &GoogleDocument{googleDriveID: driveID, googleDriveSrv: googleDriveSrv, templates: tmpl}, nil
}
