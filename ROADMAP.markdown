# ROADMAP

## Background

Our initial approach was informed by the idea that Phabriator was canonical for incident tracking,
and any tooling would be a means of automating workflow, reducing toil, and enforcing a measure of
convention.  In such a world, application state belongs to Phabricator, and Corto is stateless
orchestration.

A litmus test for whether we're true to this design would be whether or not Corto could
realistically be considered as *optional*; If Phabricator is our incident coordination software and
Corto something to reduce toil, then it should be possible —if not somewhat tedious— to just use
Phabricator directly.  However, our use of Google Docs for realtime collaboration alone upends that
expectation, as does automated updating of wikimediastatus.net (both examples of state/relationships
not modeled by Phabricator).  Planned features like arbitrary metadata is where things really go off
the rails though.  Indexing (and reindexing on change) and reporting are something that would have
to live entirely outside of Phabricator.


## Moving Forward

### Service

Corto should be a stateful, network service.

#### Model

Corto is canonical for incidents (they are modeled within Corto). In addition to state that is
unique to Corto (arbitrary metadata, for example), an incident also establishes relationships with
extant systems.  For example: A Corto incident would be associated with a Phabricator task, a Google
Doc (perhaps optionally), and a wikimediastatus.net incident (optionally).

Strawman Pseudo-DDL:

```sql
CREATE TABLE incident (
    -- Primary key, automatically incrementing integer
    id INTEGER PRIMARY KEY AUTOINCREMENT,

    -- Phabricator ID
    phabricator_id INTEGER,

    -- Link to Google Doc
    google_doc TEXT,

    -- wikimediastatus.net ID
    wikimediastatusnet_id INTEGER
);

CREATE TABLE metadata (
    -- Metadata tag
    tag TEXT,

    -- Foreign key referencing the incident table's ID
    incident_id INTEGER,

    FOREIGN KEY (incident_id) REFERENCES incident(id)
);
```

#### Controller

The service handles orchestration: So for example, creating a new incident will create a
corresponding Phabricator incident, and can (by default) create a new Google Doc, establishing a
link.

```
   ┌───────────────────┐       ┌──────────────────────────┐       ┌───────────────────┐
   │ Google            │       │ Corto                    │       │ Wikimediastatus   │
   │                   │       │                          │       │                   │
   │        ◄─ ─ ─ ─ ─ ┼ ─ ┐   │                          │   ┌ ─ ┼ ─ ─ ─ ─►          │
   │                   │   │   │                          │       │                   │
   │                   │       │                          │   │   │                   │
   │                   │   │   │  ┌──────────┐            │       │                   │
   │                   │       │  ┼──────────┼            │   │   │                   │
   │                   │   │   │  ┼──────────┼            │       │                   │
   └───────────────────┘       │  ┼──────────┼            │   │   └───────────────────┘
   ┌───────────────────┐   │   │  ┼──────────┼            │
   │ Phabricator       │       │  ┼──────────┼─ ─ ─ ─ ─ ─ ┼ ─ ┘
   │                   │   └ ─ ┼ ─┼──────────┼            │
   │         ◄─────────┼───────┼──┼──────────┼            │
   │                   │       │  └──────────┘            │
   │                   │       │                          │
   │                   │       └──────────────────────────┘
   │                   │
   │                   │
   └───────────────────┘
```


#### View

REST (please). GRPC? GraphQL? IDM authenticated[^1].


### Client(s)

Corto should have a webui.  Managing access to a webui via IDM[^1] will be straightforward, making
this a good choice as the default interface for privileged operations[^2].



[^1]: https://wikitech.wikimedia.org/wiki/IDM
[^2]: By comparison, our IRC bot layers its own access control on top of shared service credentials,
      a practice that will only become more problematic the more we attempt to do.
